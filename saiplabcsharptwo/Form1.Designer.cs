﻿namespace Sip32 {
    partial class MainForm {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent() {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.просмотрToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prevToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.студентыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nameLabel = new System.Windows.Forms.Label();
            this.surnameLabel = new System.Windows.Forms.Label();
            this.facultiLabel = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.facultiTextBox = new System.Windows.Forms.TextBox();
            this.prevButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.findLabel = new System.Windows.Forms.Label();
            this.findComboBox = new System.Windows.Forms.ComboBox();
            this.findTextbox = new System.Windows.Forms.TextBox();
            this.rovnoLabel = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.findButton = new System.Windows.Forms.Button();
            this.diplomaTextBox = new System.Windows.Forms.TextBox();
            this.diplomaCheckBox = new System.Windows.Forms.CheckBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.просмотрToolStripMenuItem,
            this.студентыToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(506, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.saveAsToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(65, 29);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(214, 30);
            this.открытьToolStripMenuItem.Text = "Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Enabled = false;
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(214, 30);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // сохранитьКакToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Enabled = false;
            this.saveAsToolStripMenuItem.Name = "сохранитьКакToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(214, 30);
            this.saveAsToolStripMenuItem.Text = "Сохранить как";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // просмотрToolStripMenuItem
            // 
            this.просмотрToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prevToolStripMenuItem,
            this.nextToolStripMenuItem});
            this.просмотрToolStripMenuItem.Name = "просмотрToolStripMenuItem";
            this.просмотрToolStripMenuItem.Size = new System.Drawing.Size(109, 29);
            this.просмотрToolStripMenuItem.Text = "Просмотр";
            // 
            // предыдущийToolStripMenuItem
            // 
            this.prevToolStripMenuItem.Enabled = false;
            this.prevToolStripMenuItem.Name = "предыдущийToolStripMenuItem";
            this.prevToolStripMenuItem.Size = new System.Drawing.Size(206, 30);
            this.prevToolStripMenuItem.Text = "Предыдущий";
            this.prevToolStripMenuItem.Click += new System.EventHandler(this.prevButton_Click);
            // 
            // следующийToolStripMenuItem
            // 
            this.nextToolStripMenuItem.Enabled = false;
            this.nextToolStripMenuItem.Name = "следующийToolStripMenuItem";
            this.nextToolStripMenuItem.Size = new System.Drawing.Size(206, 30);
            this.nextToolStripMenuItem.Text = "Следующий";
            this.nextToolStripMenuItem.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // студентыToolStripMenuItem
            // 
            this.студентыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.студентыToolStripMenuItem.Name = "студентыToolStripMenuItem";
            this.студентыToolStripMenuItem.Size = new System.Drawing.Size(100, 29);
            this.студентыToolStripMenuItem.Text = "Студенты";
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(174, 30);
            this.добавитьToolStripMenuItem.Text = "Добавить";
            this.добавитьToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // удалитьToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Enabled = false;
            this.deleteToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(174, 30);
            this.deleteToolStripMenuItem.Text = "Удалить";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // nameLabel
            // 
            this.nameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(120, 101);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(44, 20);
            this.nameLabel.TabIndex = 1;
            this.nameLabel.Text = "Имя:";
            // 
            // surnameLabel
            // 
            this.surnameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.surnameLabel.AutoSize = true;
            this.surnameLabel.Location = new System.Drawing.Point(79, 158);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(85, 20);
            this.surnameLabel.TabIndex = 2;
            this.surnameLabel.Text = "Фамилия:";
            // 
            // facultiLabel
            // 
            this.facultiLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.facultiLabel.AutoSize = true;
            this.facultiLabel.Location = new System.Drawing.Point(66, 220);
            this.facultiLabel.Name = "facultiLabel";
            this.facultiLabel.Size = new System.Drawing.Size(98, 20);
            this.facultiLabel.TabIndex = 3;
            this.facultiLabel.Text = "Факультет:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nameTextBox.Enabled = false;
            this.nameTextBox.Location = new System.Drawing.Point(182, 98);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(244, 26);
            this.nameTextBox.TabIndex = 4;
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.surnameTextBox.Enabled = false;
            this.surnameTextBox.Location = new System.Drawing.Point(182, 155);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.Size = new System.Drawing.Size(244, 26);
            this.surnameTextBox.TabIndex = 5;
            // 
            // facultiTextBox
            // 
            this.facultiTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.facultiTextBox.Enabled = false;
            this.facultiTextBox.Location = new System.Drawing.Point(182, 217);
            this.facultiTextBox.Name = "facultiTextBox";
            this.facultiTextBox.Size = new System.Drawing.Size(244, 26);
            this.facultiTextBox.TabIndex = 6;
            // 
            // prevButton
            // 
            this.prevButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.prevButton.Enabled = false;
            this.prevButton.Location = new System.Drawing.Point(104, 340);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(129, 33);
            this.prevButton.TabIndex = 7;
            this.prevButton.TabStop = false;
            this.prevButton.Text = "Предыдущий";
            this.prevButton.UseVisualStyleBackColor = true;
            this.prevButton.Click += new System.EventHandler(this.prevButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nextButton.Enabled = false;
            this.nextButton.Location = new System.Drawing.Point(284, 340);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(129, 33);
            this.nextButton.TabIndex = 8;
            this.nextButton.TabStop = false;
            this.nextButton.Text = "Следующий";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // findLabel
            // 
            this.findLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.findLabel.AutoSize = true;
            this.findLabel.Location = new System.Drawing.Point(32, 420);
            this.findLabel.Name = "findLabel";
            this.findLabel.Size = new System.Drawing.Size(59, 20);
            this.findLabel.TabIndex = 9;
            this.findLabel.Text = "Поиск:";
            // 
            // findComboBox
            // 
            this.findComboBox.FormattingEnabled = true;
            this.findComboBox.Items.AddRange(new object[] {
            "Имя",
            "Фамилия",
            "Факультет"});
            this.findComboBox.Location = new System.Drawing.Point(36, 460);
            this.findComboBox.Name = "findComboBox";
            this.findComboBox.Size = new System.Drawing.Size(197, 28);
            this.findComboBox.TabIndex = 10;
            this.findComboBox.TabStop = false;
            this.findComboBox.SelectedIndexChanged += new System.EventHandler(this.FIOcomboBox_SelectedIndexChanged);
            // 
            // findTextbox
            // 
            this.findTextbox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.findTextbox.Enabled = false;
            this.findTextbox.Location = new System.Drawing.Point(284, 460);
            this.findTextbox.Name = "findTextbox";
            this.findTextbox.Size = new System.Drawing.Size(190, 26);
            this.findTextbox.TabIndex = 11;
            this.findTextbox.TabStop = false;
            this.findTextbox.TextChanged += new System.EventHandler(this.findTextbox_TextChanged);
            this.findTextbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.findTextbox_KeyDown);
            // 
            // rovnoLabel
            // 
            this.rovnoLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rovnoLabel.AutoSize = true;
            this.rovnoLabel.Location = new System.Drawing.Point(249, 462);
            this.rovnoLabel.Name = "rovnoLabel";
            this.rovnoLabel.Size = new System.Drawing.Size(18, 20);
            this.rovnoLabel.TabIndex = 12;
            this.rovnoLabel.Text = "=";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // findButton
            // 
            this.findButton.Enabled = false;
            this.findButton.Location = new System.Drawing.Point(36, 508);
            this.findButton.Name = "findButton";
            this.findButton.Size = new System.Drawing.Size(438, 36);
            this.findButton.TabIndex = 13;
            this.findButton.Text = "Искать";
            this.findButton.UseVisualStyleBackColor = true;
            this.findButton.Click += new System.EventHandler(this.findButton_Click);
            // 
            // diplomaTextBox
            // 
            this.diplomaTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.diplomaTextBox.Enabled = false;
            this.diplomaTextBox.Location = new System.Drawing.Point(182, 274);
            this.diplomaTextBox.Name = "diplomaTextBox";
            this.diplomaTextBox.Size = new System.Drawing.Size(244, 26);
            this.diplomaTextBox.TabIndex = 100;
            this.diplomaTextBox.TabStop = false;
            // 
            // diplomaCheckBox
            // 
            this.diplomaCheckBox.AutoSize = true;
            this.diplomaCheckBox.Enabled = false;
            this.diplomaCheckBox.Location = new System.Drawing.Point(7, 276);
            this.diplomaCheckBox.Name = "diplomaCheckBox";
            this.diplomaCheckBox.Size = new System.Drawing.Size(157, 24);
            this.diplomaCheckBox.TabIndex = 16;
            this.diplomaCheckBox.Text = "Номер диплома";
            this.diplomaCheckBox.UseVisualStyleBackColor = true;
            this.diplomaCheckBox.Enter += new System.EventHandler(this.diplomaCheckBox_Enter);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 584);
            this.Controls.Add(this.diplomaCheckBox);
            this.Controls.Add(this.diplomaTextBox);
            this.Controls.Add(this.findButton);
            this.Controls.Add(this.rovnoLabel);
            this.Controls.Add(this.findTextbox);
            this.Controls.Add(this.findComboBox);
            this.Controls.Add(this.findLabel);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.prevButton);
            this.Controls.Add(this.facultiTextBox);
            this.Controls.Add(this.surnameTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.facultiLabel);
            this.Controls.Add(this.surnameLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "mainForm";
            this.Text = "Студенты";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem просмотрToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem студентыToolStripMenuItem;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label surnameLabel;
        private System.Windows.Forms.Label facultiLabel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox surnameTextBox;
        private System.Windows.Forms.TextBox facultiTextBox;
        private System.Windows.Forms.Button prevButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Label findLabel;
        private System.Windows.Forms.ComboBox findComboBox;
        private System.Windows.Forms.TextBox findTextbox;
        private System.Windows.Forms.Label rovnoLabel;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prevToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nextToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button findButton;
        private System.Windows.Forms.TextBox diplomaTextBox;
        private System.Windows.Forms.CheckBox diplomaCheckBox;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

