﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Runtime.Serialization.Formatters.Binary;

namespace Sip32 {
    public partial class MainForm : Form {
        //Переменные
        //Список студентов
        List<Student> students = new List<Student>();
        //index выбранного студента
        int index = -1;
        //Адрес рабочего xml
        string linkXml = "";
        //True если ничего не сохранено
        bool notSave = false;
        //Старый поисковый индекс для поиска нескольких штук
        int oldFindIndex = -1;

        /// <summary>
        /// Добавление пустой записи о студенте
        /// </summary>
        public void AddNewElem() {
            var student = new Student("", "", "");
            students.Add(student);
        }

        /// <summary>
        /// Изменение текущей записи index, данные берутся из ComboBox
        /// </summary>
        public void EditInfoAboutStudent() {
            students[index].name = nameTextBox.Text;
            students[index].surname = surnameTextBox.Text;
            students[index].faculti = facultiTextBox.Text;
            try {
                var temp = (SuperStudent)students[index];
                temp.diplomaId = Convert.ToInt32(diplomaTextBox.Text);
                students[index] = temp;
            } catch (Exception e) {
            }
            notSave = true;
        }

        /// <summary>
        /// Заполнение ComboBox текущими значениями в зависимости от index
        /// </summary>
        public void UpdateComboBox() {
            nameTextBox.Text = students[index].name;
            surnameTextBox.Text = students[index].surname;
            facultiTextBox.Text = students[index].faculti;
            diplomaTextBox.Text = "";
            try {
                SuperStudent temp = (SuperStudent)students[index];
                Console.WriteLine(index);
                diplomaTextBox.Text = temp.diplomaId.ToString();
                diplomaCheckBox.Checked = true;
                //diplomaCheckBox.Enabled = false;
                diplomaTextBox.Enabled = true;
            } catch (Exception e) {
            }
            this.ActiveControl = rovnoLabel;
        }

        /// <summary>
        /// Прыжок в элемент списка index
        /// </summary>
        /// <param name="currantIndex">Элемент, к которому надо отправиться</param>
        public void SetComboBoxAt(int currantIndex) {
            index = currantIndex;
            UpdateComboBox();
            if (index == 0) {
                prevButton.Enabled = false;
                prevToolStripMenuItem.Enabled = false;
            } else {
                prevButton.Enabled = true;
                prevToolStripMenuItem.Enabled = true;
            }
            if (students.Count == (index + 1)) {
                nextButton.Enabled = false;
                nextToolStripMenuItem.Enabled = false;
            } else {
                nextButton.Enabled = true;
                nextToolStripMenuItem.Enabled = true;
            }

        }

        /// <summary>
        /// Красивый вывод ответа
        /// </summary>
        /// <param name="caption">Заголовок</param>
        /// <param name="message">Текст</param>
        /// <param name="e">Тип иконки (смотри документацию)</param>
        private void ShowAlert(string caption, string message, MessageBoxIcon e) {
            var result = MessageBox.Show(message, caption,
                             MessageBoxButtons.OK,
                             e);
        }

        public MainForm() {
            InitializeComponent();
            this.ActiveControl = rovnoLabel;
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e) {
            if (index != -1) {
                EditInfoAboutStudent();
                prevButton.Enabled = true;
                prevToolStripMenuItem.Enabled = true;
            }
            nameTextBox.Text = "";
            surnameTextBox.Text = "";
            facultiTextBox.Text = "";
            diplomaTextBox.Text = "";
            diplomaTextBox.Enabled = false;
            diplomaCheckBox.Checked = false;
            nameTextBox.Enabled = true;
            surnameTextBox.Enabled = true;
            facultiTextBox.Enabled = true;
            deleteToolStripMenuItem.Enabled = true;
            diplomaCheckBox.Enabled = true;
            saveAsToolStripMenuItem.Enabled = true;
            AddNewElem();
            index = students.Count - 1;
            nextButton.Enabled = false;
            nextToolStripMenuItem.Enabled = false;

        }

        private void prevButton_Click(object sender, EventArgs e) {
            EditInfoAboutStudent();
            index--;
            nextButton.Enabled = true;
            nextToolStripMenuItem.Enabled = true;
            diplomaCheckBox.Checked = false;
            diplomaCheckBox.Enabled = true;
            diplomaTextBox.Enabled = false;
            UpdateComboBox();
            if (index != 0) return;
            prevButton.Enabled = false;
            prevToolStripMenuItem.Enabled = false;
        }

        private void nextButton_Click(object sender, EventArgs e) {
            EditInfoAboutStudent();
            index++;
            prevButton.Enabled = true;
            prevToolStripMenuItem.Enabled = true;
            diplomaCheckBox.Checked = false;
            diplomaCheckBox.Enabled = true;
            diplomaTextBox.Enabled = false;
            UpdateComboBox();
            if (students.Count != (index + 1)) return;
            nextButton.Enabled = false;
            nextToolStripMenuItem.Enabled = false;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
            EditInfoAboutStudent();
            var serializer = new XmlSerializer(typeof(List<Student>));
            using (TextWriter writer = new StreamWriter(linkXml)) serializer.Serialize(writer, students);
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e) {
            if (notSave) {
                var m = MessageBox.Show("Файл был изменен. Хотите сохранить?", "Студенты", MessageBoxButtons.YesNoCancel);
                switch (m)
                {
                    case DialogResult.Cancel:
                        notSave = true;
                        return;
                    case DialogResult.Yes:
                        notSave = false;
                        saveToolStripMenuItem_Click(sender, e);
                        break;
                    case DialogResult.No:
                        notSave = false;
                        break;
                }
            }
            openFileDialog1.ShowDialog();
            linkXml = openFileDialog1.FileName;

            var deserializer = new XmlSerializer(typeof(List<Student>));
            TextReader reader = new StreamReader(linkXml);
            var obj = deserializer.Deserialize(reader);
            students = null;
            students = new List<Student>();
            index = -1;
            students = (List<Student>)obj;
            reader.Close();

            prevButton.Enabled = false;
            prevToolStripMenuItem.Enabled = false;
            diplomaCheckBox.Enabled = true;
            saveAsToolStripMenuItem.Enabled = true;
            сохранитьToolStripMenuItem.Enabled = true;
            if (students[0] != null) {
                index++;
                UpdateComboBox();
                nameTextBox.Enabled = true;
                surnameTextBox.Enabled = true;
                facultiTextBox.Enabled = true;
                deleteToolStripMenuItem.Enabled = true;
            }
            if (students[1] == null) return;
            nextButton.Enabled = true;
            nextToolStripMenuItem.Enabled = true;
        }

        private void FIOcomboBox_SelectedIndexChanged(object sender, EventArgs e) {
            findTextbox.Enabled = true;
        }

        private void findTextbox_TextChanged(object sender, EventArgs e) {
            findButton.Enabled = true;
            oldFindIndex = -1;
        }

        private void findButton_Click(object sender, EventArgs e) {
            var finishedIndex = -1;
            if (students.Count == 0)
                ShowAlert("Внимание!", "В данный момент не открыт никакой файл", MessageBoxIcon.Warning);
            else {
                var answer = 0;
                switch (findComboBox.SelectedIndex) {
                    case 0:
                        answer = 0;
                        foreach (var ii in students) {
                            if (ii.name == findTextbox.Text && answer > oldFindIndex) {
                                finishedIndex = answer;
                                oldFindIndex = finishedIndex;
                                break;
                            }
                            answer++;
                        }
                        if (answer == students.Count) {
                            refind();
                            return;
                        }
                        break;
                    case 1:
                        answer = 0;
                        foreach (var ii in students) {
                            if (ii.surname == findTextbox.Text && answer > oldFindIndex) {
                                finishedIndex = answer;
                                oldFindIndex = finishedIndex;
                                break;
                            }
                            answer++;
                        }
                        if (answer == students.Count) {
                            refind();
                            return;
                        }
                        break;
                    case 2:
                        answer = 0;
                        foreach (var ii in students) {
                            if (ii.faculti == findTextbox.Text && answer > oldFindIndex) {
                                finishedIndex = answer;
                                oldFindIndex = finishedIndex;
                                break;
                            }
                            answer++;
                        }
                        if (answer == students.Count) {
                            refind();
                            return;
                        }
                        break;
                    default:
                        Console.WriteLine("Я не знаю как ты это сделал!");
                        break;
                }
                if (finishedIndex == -1)
                    ShowAlert("Внимание!", "Элемент не найден", MessageBoxIcon.Warning);
                else
                    SetComboBoxAt(finishedIndex);
            }
        }

        private void findTextbox_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter)
                findButton_Click(sender, e);
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e) {
            notSave = true;
            nameTextBox.Text = "";
            surnameTextBox.Text = "";
            facultiTextBox.Text = "";
            diplomaTextBox.Text = "";
            diplomaTextBox.Enabled = false;
            diplomaCheckBox.Checked = false;
            students.RemoveAt(index);
            if (index > 0) {
                index--;
                SetComboBoxAt(index);
            } else if (students.Count > (index + 1)) {
                //index++;
                SetComboBoxAt(index);
            } else {
                deleteToolStripMenuItem.Enabled = false;
                nameTextBox.Enabled = false;
                surnameTextBox.Enabled = false;
                facultiTextBox.Enabled = false;
                diplomaTextBox.Enabled = false;
                prevButton.Enabled = false;
                nextButton.Enabled = false;
                nextToolStripMenuItem.Enabled = false;
                diplomaCheckBox.Enabled = false;
                diplomaCheckBox.Checked = false;
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e) {
            saveFileDialog1.Filter = "Xml documents | *.xml";
            saveFileDialog1.DefaultExt = "xml";
            saveFileDialog1.ShowDialog();
            linkXml = saveFileDialog1.FileName;
            сохранитьToolStripMenuItem.Enabled = true;
            saveToolStripMenuItem_Click(sender, e);
        }

        private void diplomaCheckBox_Enter(object sender, EventArgs e) {
            if (diplomaCheckBox.Checked) {
                diplomaTextBox.Enabled = false;
                diplomaTextBox.Text = "";
                var temp = new Student(students[index].name, students[index].surname, students[index].faculti);
                students[index] = temp;
            } else {
                diplomaTextBox.Enabled = true;
                var temp = new SuperStudent(students[index].name, students[index].surname, students[index].faculti, -1);
                students[index] = temp;
            }
        }

        private void mainForm_FormClosing(object sender, FormClosingEventArgs e) {
            if (!notSave) return;
            notSave = false;
            var m = MessageBox.Show("Файл был изменен. Хотите сохранить?", "Студенты", MessageBoxButtons.YesNoCancel);
            switch (m)
            {
                case DialogResult.Cancel:
                    e.Cancel = true;
                    break;
                case DialogResult.Yes:
                    saveToolStripMenuItem_Click(sender, e);
                    break;
            }
        }

        public void refind() {
            var m = MessageBox.Show("Поиск достиг отправной точки. Начать с начала?", "Студенты", MessageBoxButtons.YesNo);
            if (m != DialogResult.Yes) return;
            oldFindIndex = -1;
            findButton_Click(new object(), new EventArgs());
        }
    }

    [Serializable]
    public class SuperStudent : Student {
        [XmlElement("Diploma")]
        public int diplomaId;
        /// <summary>
        /// Конструктор класса superStudent
        /// </summary>
        /// <param name="name">Имя</param>
        /// <param name="surname">Фамилия</param>
        /// <param name="faculti">Факультет</param>
        /// <param name="diplomaId">Номер диплома</param>
        public SuperStudent(string name, string surname, string faculti, int diplomaId) : base(name, surname, faculti) {
            this.name = name;
            this.surname = surname;
            this.faculti = faculti;
            this.diplomaId = diplomaId;
        }
        public SuperStudent() {
        }
    }

    [XmlInclude(typeof(SuperStudent))]
    [Serializable]
    public class Student {
        [XmlElement("Name")]
        public string name;
        [XmlElement("Surname")]
        public string surname;
        [XmlElement("Faculti")]
        public string faculti;
        /// <summary>
        /// Конструктор класса student
        /// </summary>
        /// <param name="name">Имя</param>
        /// <param name="surname">Фамилия</param>
        /// <param name="faculti">Факультет</param>
        public Student (string name, string surname, string faculti) {
            this.name = name;
            this.surname = surname;
            this.faculti = faculti;
        }

        public Student () {
        }
    }
}
